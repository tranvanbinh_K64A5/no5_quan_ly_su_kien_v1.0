<?php
class Controller {
    protected function render($view, $data=[]) {
        require_once _DIR_ROOT.'/app/view/'.$view.'.php';
    }

    protected function models($models) {
        require_once _DIR_ROOT.'/app/model/'.$models.'.php';
        return new $models;
    }
}
?>
<?php
class Event extends Controller {

    private $event;
    public function __construct()
    {
        $this->event = $this->models('EventModel');
    }

    public function index()
    {
        $rs = $this->event->select_array();

        $this->render('masterlayout', [
            'page' => 'event/search',
           'array' => $rs,
        ]);
    }

    public function search()
    {
        $keySearch = $_GET['key_search'];
        if(session_id() == '') {
            session_start();
        }
        $_SESSION["keyword_event"] = $keySearch;
        $res = $this->event->searchEvent($keySearch);

        $this->render('masterlayout', [
            'page' => 'event/search',
            'array' => $res,
        ]);
    }

    public function delete()
    {
        $id = $_POST['id'];
        $res = $this->event->deleteEvent($id);
        if ($res) {
            echo "successfully";
        }
    }

    public function update($id)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            //xác nhận lần đầu
            if (isset($_POST['btnxacnhansuaevent'])) {
                $data = $this->event->select_one("*", ["id" => $id]);
                $error = array();
                //kiem tra hinh anh
                if (!empty($_FILES['event_avatar_for_update']['name'])) {
                    $dir = "web/avatar";
                    if (!file_exists($dir)) {
                        mkdir($dir);
                    }
                    $target_dir = "web/avatar/";
                    $extension_file = pathinfo($_FILES['event_avatar_for_update']['name'], PATHINFO_EXTENSION);
                    if ($extension_file != "jpg" && $extension_file != "png" && $extension_file != "jpeg" && $extension_file != "gif") {
                        $error['extension_file'] = 'true';
                    }
                    $basename_file = pathinfo($_FILES['event_avatar_for_update']['name'], PATHINFO_FILENAME);
                    $target_file = $target_dir . $basename_file . '_' . date("YmdHis") . '.' . $extension_file;
                    if (!file_exists($target_file)) {
                        move_uploaded_file($_FILES["event_avatar_for_update"]["tmp_name"], $target_file);
                    }
                    $_SESSION["event_avatar_for_update"] = $target_file;
                    $_SESSION['image_old'] = $data['avatar'];
                } else {
                    $_SESSION["event_avatar_for_update"] = $data['avatar'];
                }
                // echo "Gia tri cua bien target_file: ".$target_file."<br>";
                ///////////////////////////////////////////////
                // xu li validate
                if (empty(trim($_POST['name_update_form']))) {
                    $error['name_update_form'] = 'Hãy nhập tên sự kiện';
                } else if (strlen($_POST['name_update_form']) > 100) {
                    $error['name_update_form'] = 'Không nhập quá 100 ký tự';
                }
                if (empty(trim($_POST['slogan_update_form']))) {
                    $error['slogan_update_form'] = 'Hãy nhập slogan';
                } else if (strlen($_POST['slogan_update_form']) > 250) {
                    $error['slogan_update_form'] = 'Không nhập quá 250 ký tự';
                }
                if (empty(trim($_POST['leader_update_form']))) {
                    $error['leader_update_form'] = 'Hãy nhập tên leader';
                } else if (strlen($_POST['leader_update_form']) > 250) {
                    $error['leader_update_form'] = 'Không nhập quá 250 ký tự';
                }
                if (empty(trim($_POST['description_update_form']))) {
                    $error['description_update_form'] = 'Hãy nhập tên mô tả chi tiết';
                } else if (strlen($_POST['description_update_form']) > 1000) {
                    $error['description_update_form'] = 'Không nhập quá 1000 ký tự';
                }
                if (!empty($_FILES['event_avatar_update_form']['name']) && !empty($data['avatar'])) {
                    $error['event_avatar_update_form'] = 'Hãy chọn avatar';
                } else if (!empty($error['extension_file'])) {
                    $error['event_avatar_update_form'] = 'Định dạng ảnh không đúng';
                }
                // get data from form data
                $data['name'] = $_POST['name_update_form'] ? $_POST['name_update_form'] : '';
                $data['slogan'] = $_POST['slogan_update_form'] ? $_POST['slogan_update_form'] : '';
                $data['leader'] = $_POST['leader_update_form'] ? $_POST['leader_update_form'] : '';
                $data['description'] = $_POST['description_update_form'] ? $_POST['description_update_form'] : '';
                // $data['avatar'] = $_FILES['event_avatar_for_update'] ? $_FILES['event_avatar_for_update'] : '';
                if (count($error) != 0) {
                    $this->render('masterlayout', [
                        'page' => 'event/event_modify_input',
                        'data' => $data,
                        'error' => $error,
                    ]);
                } else {
                    $this->render('masterlayout', [
                        'page' => 'event/event_modify_confirm',
                        'data' => $data,
                    ]);
                }

            }

            //preview xác nhận lần 2 và tiến hành update
            if (isset($_POST['btnxacnhansuaevent2'])) {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $this->event->update(
                    [
                        "name" => trim($_SESSION["name_update_form"]),
                        "slogan" => $_SESSION["slogan_update_form"],
                        "avatar" => $_SESSION["event_avatar_for_update"],
                        "leader" => $_SESSION["leader_update_form"],
                        "description" => trim($_SESSION["description_update_form"]),
                        "updated" => date('Y/m/d H:i:s', time()),
                    ],
                    ["id" => $id]
                );
                // thực hiện xoá ảnh cũ nếu ko update ảnh
                if (isset($_SESSION['image_old'])) {
                    if (file_exists(_DIR_ROOT . '/' . $_SESSION['image_old'])) {
                        unlink(_DIR_ROOT . '/' . $_SESSION['image_old']);
                    }
                }
                $this->render('masterlayout', [
                    'page' => 'event/event_modify_success',
                ]);

                $_SESSION['image_old'] = $_SESSION["event_avatar_for_update"] = $_SESSION["slogan_update_form"] = $_SESSION["leader_update_form"] = $_SESSION["name_update_form"] = $_SESSION["description_update_form"] = null;
            }

        }
        $data = $this->event->select_one("*", ["id" => $id]);
        $this->render('masterlayout', [
            'page' => 'event/event_modify_input',
            'data' => $data,
        ]);
    }

    public function add()
    {
        $fail = false;
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $slogan = $_POST['slogan'];
            $leader = $_POST['leader'];
            $avatar = $_FILES['avatar'];
            // $type = $_POST['type'];
            // $event_id = $_POST['event_id'];
            $description = $_POST['description'];
            // $duplicate = $this->eventModel->checkDuplicate($event_id);
            if (strlen($name) > 100) {
                $fail = 'Không nhập quá 100 ký tự';
            }
            if (!$fail and strlen($slogan) > 250) {
                $fail = 'Không nhập quá 250 ký tự';
            }
            if (!$fail and strlen($leader) > 250) {
                $fail = 'Không nhập quá 250 ký tự';
            }
            if (!$fail and strlen($description) > 1000) {
                $fail = 'Không nhập quá 1000 ký tự';
            }
            if (!$fail and isset($_FILES['avatar'])) {

                $target_dir = __DIR__ . "/../../web/avatar/";

                $allowUpload = true;

                $fileType = pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);
                $fileName = $name . '_' . "." . $fileType;
                $target_file = $target_dir . $fileName;
                $urlDir = "web/avatar/";

                $maxfilesize   = 10000000;


                if ($fail == false and $_FILES["avatar"]['error'] != 0) {
                    $fail = "The uploaded file is error or no file selected.";
                    die;
                }


                if ($fail == false and $_FILES["avatar"]["size"] > $maxfilesize) {
                    $fail = "Size of the uploaded file must be smaller than $maxfilesize bytes.";
                    $allowUpload = false;
                }


                if ($fail == false and file_exists($target_file)) {
                    $fail = "The file name already exists on the server.";
                    $allowUpload = false;
                }

                if ($fail == false and $allowUpload) {
                    $created = date('Y-m-d H:i:s');
                    $updated = date('Y-m-d H:i:s');

                    if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {
                        $avatar = $urlDir . $fileName;
                        $data = array(
                            'name' => $name,
                            'slogan' => $slogan,
                            'leader' => $leader,
                            'description' => $description,
                            'avatar' => $avatar,
                            'created' => $created,
                            'updated' => $updated
                        );
                        $rs = $this->event->insert($data);
                        if (!$rs) {
                            $fail = "An error occurred while inserting the data.";
                        } else {
                            $this->add_success();
                        }
                    } else {
                        $fail = "An error occurred while uploading the file.";
                    }
                }
            }
        }
        $this->render('masterlayout', [
            'page' => 'event/create',
            'fail' => $fail
        ]);
    }

    public function add_success()
    {
        $this->render('masterlayout', [
            'page' => 'event/create_success',
        ]);
    }
}
?>
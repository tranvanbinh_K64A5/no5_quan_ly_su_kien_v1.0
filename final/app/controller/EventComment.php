<?php
class EventComment extends Controller {
    private $event;
    private $event_comments;

    public function __construct() {
        $this->event = $this->models('EventModel');
        $this->event_comments = $this->models('EventCommentModel');
    }

    public function index($id=-1) {
        if ($id === -1) $this->loadError();
        else {
            $rs_event = $this->event->getEventById($id);
            $rs_comments = $this->event_comments->select_array('*', ['event_id' => $id]);

            $this->render('masterlayout', [
                'page' => 'event_comments/index',
               'event' => $rs_event,
               'comments' => $rs_comments
            ]);
        }
    }

    // Get String of date-time
    function getStringOfDate() {
        $date   = new DateTime(); //this returns the current date time
        $result = $date->format('Y-m-d-H-i-s');
        $krr    = explode('-', $result);
        $result = implode("", $krr);
        return $result;
    }

    function saveFile($file, $des) {
        $filename = $file["tmp_name"]; // Get file
        $originalName = pathinfo($file["name"], PATHINFO_FILENAME); // Get original file name
        $extension = pathinfo($file["name"], PATHINFO_EXTENSION); // Get extension name of file
        $destination = $des . $originalName . "_" . $this->getStringOfDate() . "." . $extension; // Path to save image
        move_uploaded_file($filename, $destination); // Save uploaded picture in your directory

        $url = str_replace(_DIR_ROOT.'/', "",$destination);
        return $url;
    }

    public function add($id) {
        $input_valid = true;
        $error_messages = ['avatar' => '', 'content' => ''];

        if (isset($_POST['submit_comment'])) {
            if(empty($_FILES["avatar"]['name'])) {
                $error_messages['avatar'] = "Hãy chọn avatar!";
                $input_valid = false;
            }
            if(empty($_POST["content"])) {
                $error_messages['content'] = "Hãy nhập nội dung comment!";
                $input_valid = false;
            } elseif (strlen($_POST["content"]) > 1000) {
                $error_messages['content'] = "Không nhập quá 1000 ký tự!";
                $input_valid = false;
            }

            if ($input_valid) {
                $avatar = $_FILES["avatar"];
                $content = $_POST["content"];

                $avatarUrl = $this->saveFile($avatar, _DIR_ROOT."/web/avatar/tmp/");
                
                $this->render('masterlayout', [
                    'page' => 'event_comments/confirm_comment',
                    'event_id' => $id,
                    'avatarUrl' => $avatarUrl,
                    'content' => $content
                ]);

                
            } else {
                $this->render('masterlayout', [
                    'page' => 'event_comments/add_comment',
                    'event_id' => $id,
                    'error_messages' => $error_messages
                ]);
            }

        } else {
            $this->render('masterlayout', [
                'page' => 'event_comments/add_comment',
                'event_id' => $id
            ]);
        }
    }

    public function confirm_add() {
        if (isset($_POST['confirm_add'])) {
            $event_id = $_POST["event_id"];
            $content = $_POST["content"];

            $avatarUrl = $_POST["avatarUrl"];
            $absolute_path = _DIR_ROOT."/".$avatarUrl;
            $new_absolute_path = str_replace("/tmp", "", $absolute_path);
            rename($absolute_path, $new_absolute_path); // move file

            $new_avatarUrl = str_replace(_DIR_ROOT.'/', "", $new_absolute_path);
            
            // Save to DB
            $rs = $this->event_comments->insert(['event_id' => $event_id, 'avatar' => $new_avatarUrl, 'content' => $content]);
            if ($rs) {
                $this->render('masterlayout', [
                    'page' => 'event_comments/add_success',
                    'event_id' => $event_id
                ]);
            } else {
                $this->loadError();
            }

        }
    }

    // public function update($id) {
    //     echo "Update ".$id;
    // }
     public function update($id)
        {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                //xác nhận lần đầu
                if (isset($_POST['btnxacnhansua'])) {
                    $data = $this->event_comments->select_one("*", ["id" => $id]);
                    $error = array();
                    //kiem tra hinh anh
                    if (!empty($_FILES['avatar_for_update']['name'])) {
                        $dir = "web/avatar";
                        if (!file_exists($dir)) {
                            mkdir($dir);
                        }
                        $target_dir = "web/avatar/";
                        $extension_file = pathinfo($_FILES['avatar_for_update']['name'], PATHINFO_EXTENSION);
                        if ($extension_file != "jpg" && $extension_file != "png" && $extension_file != "jpeg" && $extension_file != "gif") {
                            $error['extension_file'] = 'true';
                        }
                        $basename_file = pathinfo($_FILES['avatar_for_update']['name'], PATHINFO_FILENAME);
                        $target_file = $target_dir . $basename_file . '_' . date("YmdHis") . '.' . $extension_file;
                        if (!file_exists($target_file)) {
                            move_uploaded_file($_FILES["avatar_for_update"]["tmp_name"], $target_file);
                        }
                        $_SESSION["avatar_for_update"] = $target_file;
                        $_SESSION['image_old'] = $data['avatar'];
                    } else {
                        $_SESSION["avatar_for_update"] = $data['avatar'];
                    }
                    if (empty(trim($_POST['content_update_form']))) {
                        $error['content_update_form'] = 'Hãy nhập tên mô tả chi tiết';
                    } else if (strlen($_POST['content_update_form']) > 1000) {
                        $error['content_update_form'] = 'Không nhập quá 1000 ký tự';
                    }
                    if (!empty($_FILES['avatar_update_form']['name']) && !empty($data['avatar'])) {
                        $error['avatar_update_form'] = 'Hãy chọn avatar';
                    } else if (!empty($error['extension_file'])) {
                        $error['avatar_update_form'] = 'Định dạng ảnh không đúng';
                    }
                    $data['content'] = $_POST['content_update_form'] ? $_POST['content_update_form'] : '';
                    if (count($error) != 0) {
                        $this->render('masterlayout', [
                            'page' => 'event_comments/modify_comment',
                            'data' => $data,
                            'error' => $error,
                        ]);
                    } else {
                        $this->render('masterlayout', [
                            'page' => 'event_comments/modify_confirm_comment',
                            'data' => $data,
                        ]);
                    }
                }
                //preview xác nhận lần 2 và tiến hành update
                if (isset($_POST['btnxacnhansuacomment'])) {
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $this->event_comments->update(
                        [
                            "avatar" => $_SESSION["avatar_for_update"],
                            "content" => trim($_SESSION["content_update_form"]),
                            
                        ],
                        ["id" => $id]
                    );
                    // thực hiện xoá ảnh cũ nếu ko update ảnh
                    if (isset($_SESSION['image_old'])) {
                        if (file_exists(_DIR_ROOT . '/' . $_SESSION['image_old'])) {
                            unlink(_DIR_ROOT . '/' . $_SESSION['image_old']);
                        }
                    }
                    $data = $this->event_comments->select_one("*", ["id" => $id]);
                    $this->render('masterlayout', [
                        'page' => 'event_comments/modify_success_comment',
                        'data' => $data,
                    ]);
                    $_SESSION['image_old'] = $_SESSION["avatar_for_update"] = $_SESSION["content_update_form"] = null;
                }

            }
            $data = $this->event_comments->select_one("*", ["id" => $id]);
            $this->render('masterlayout', [

                'page' => 'event_comments/modify_comment',
                'data' => $data,
            ]);
        }
    
    public function loadError($name='404') {
        require_once _DIR_ROOT."/app/errors/".$name.".php";
    }
}
?>
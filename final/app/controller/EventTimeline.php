<?php
class EventTimeline extends Controller {
    private $event;
    private $event_timelines;

    public function __construct() {
        $this->event            = $this->models('EventModel');
        $this->event_timelines   = $this->models('EventTimelineModel');
    }

    public function index($id=-1) {
        if ($id === -1) $this->loadError();
        else {
            $rs_event = $this->event->getEventById($id);
            $rs_timelines = $this->event_timelines->select_array('*', ['event_id' => $id]);

            $this->render('masterlayout', [
                'page' => 'event_timelines/index',
                'event' => $rs_event,
                'timelines' => $rs_timelines
            ]);
        }
    }

    public function add($id) {
        $input_valid = true;
        $error_messages = ['start_date' => '', 'end_date' => '', 'name' => '', 'description' => '', 'poc_name' => ''];

        if (isset($_POST['submit_timeline'])) { 
            if(empty($_POST["start_date"])) {
                $error_messages['start_date'] = "Hãy chọn ngày bắt đầu!";
                $input_valid = false;
            }
            if(empty($_POST["end_date"])) {
                $error_messages['end_date'] = "Hãy chọn ngày kết thúc!";
                $input_valid = false;
            }
            if(empty($_POST["name"])) {
                $error_messages['name'] = "Nhập tên lịch trình!";
                $input_valid = false;
            }elseif (strlen($_POST["start_date"]) > 100) {
                $error_messages['start_date'] = "Không nhập quá 1000 ký tự!";
                $input_valid = false;
            }
            if(empty($_POST["description"])) {
                $error_messages['description'] = "Nhập nội dung!";
                $input_valid = false;
            }elseif (strlen($_POST["start_date"]) > 1000) {
                $error_messages['start_date'] = "Không nhập quá 1000 ký tự!";
                $input_valid = false;
            }
            if(empty($_POST["poc_name"])) {
                $error_messages['poc_name'] = "Nhập tên Người chịu trách nhiệm!";
                $input_valid = false;
            }elseif (strlen($_POST["start_date"]) > 100) {
                $error_messages['start_date'] = "Không nhập quá 1000 ký tự!";
                $input_valid = false;
            }

            if ($input_valid) {
                $start_date = $_POST["start_date"];
                $end_date = $_POST["end_date"];
                $name = $_POST["name"];
                $description = $_POST["description"];
                $poc_name = $_POST["poc_name"];

                $data = [
                    "start_date" => $start_date,
                    "end_date" => $end_date,
                    "name" => $name,
                    "description" => $description,
                    "poc_name" => $poc_name,
                ];
                
                $this->render('masterlayout', [
                    'page' => 'event_timelines/confirm',
                    'event_id' => $id,
                    'data' => $data
                ]);

                
            } else {
                $this->render('masterlayout', [
                    'page' => 'event_timelines/create',
                    'event_id' => $id,
                    'error_messages' => $error_messages
                ]);
            }

        } else {
            $this->render('masterlayout', [
                'page' => 'event_timelines/create',
                'event_id' => $id
            ]);
        }
    }
    // public function index($id) {
    //     echo 'id: '.$id;
    // }

    public function confirm_add() {
        if (isset($_POST['confirm_add'])) {
            $event_id = $_POST["event_id"];
            $start_date = str_replace('T', ' ', $_POST["start_date"]);
            $end_date = str_replace('T', ' ', $_POST["end_date"]);
            $name = $_POST["name"];
            $description = $_POST["description"];
            $poc_name = $_POST["poc_name"];

            // print($start_date);
            // print($end_date);
            // print($name);
            // print($description);
            // print($poc_name);
            // print_r($this->event_timelines);
            // Save to DB
            $rs = $this->event_timelines->insert(['event_id' => $event_id, 'schedule_from' => $start_date, 'schedule_to' => $end_date, 'name' => $name, 'detail ' => $description, 'poc ' => $poc_name, 'created' => date('Y/m/d H:i:s', time())]);
            
            // $rs = $this->event_timelines->insert(['event_id' => $event_id, 'from' => $start_date]);
            
            if ($rs) {
                $this->render('masterlayout', [
                    'page' => 'event_timelines/success'
                ]);
            } else {
                $this->loadError();
            }

        }
    }

    public function loadError($name='404') {
        require_once _DIR_ROOT."/app/errors/".$name.".php";
    }

    public function update($id) {
        $data = $this->event_timelines->select_one("*",["id"=>$id]);
        $this->render('masterlayout', [
            'page' => 'event_timelines/timelines_modifying',
            'data' => $data
        ]);
        if(isset($_POST['submit_modify'])) {

            $start_date = str_replace('T', ' ', $_POST["start_date"]);
            $end_date = str_replace('T', ' ', $_POST["end_date"]);
            $name = $_POST["name"];
            $detail = $_POST["detail"];
            $poc_name = $_POST["poc_name"];

            $this->event_timelines->update(
                    [
                    "schedule_from" => $start_date,
                    "schedule_to" => $end_date,
                    "name" => $name,
                    "detail" => $detail,
                    "PoC" => $poc_name,
                    "updated"=> date('Y/m/d H:i:s', time())
                    ],["id"=>$id]
                );
            $data = $this->event_timelines->select_one("*",["id"=>$id]);
            header('Location: http://localhost/final/eventtimeline/index/'. $data['event_id']);
        }else {
            $data = $this->event_timelines->select_one("*",["id"=>$id]);
            $this->render('masterlayout', [
                'page' => 'event_timelines/timelines_modifying',
                'data' => $data,
            ]);
        }
    }
}
?>

<?php
class Home extends Controller{

    public function index() {
        if(!isset($_SESSION['authen'])){
            header('location: http://localhost/final/login');
        } else {
            $this->render('masterlayout', [
                'page' => 'home/index'
            ]); 
        }
    }
}
?>
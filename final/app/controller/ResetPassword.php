<?php
class ResetPassword extends Controller{
	public $AdminModel;

    function __construct() {
        $this->AdminModel = $this->models('AdminModel');
    }
	
    public function index() {
		$error = array();
		
		$records = $this->AdminModel->getRequestedUsers();
		if (!empty($records)) {
			foreach ($records as $row) {
				$userinput = $row["login_id"];
				$resetbtn = 'reset_'.$row["login_id"];
				if (isset($_POST[$resetbtn])) {
					$input_valid = true;
					if (empty($_POST[$userinput])) {
						$error[$userinput] = "Hãy nhập mật khẩu mới";
						$input_valid = false;
					} elseif (strlen($_POST[$userinput])<6) {
						$error[$userinput]="Hãy nhập mật khẩu tối thiểu 6 ký tự";
						$input_valid = false;
					}
					
					if ($input_valid) {
						$this->AdminModel->updatePassword($userinput,$_POST[$userinput]);
						header('location:http://localhost/final');
					}
				}
			}
		}
	
		$this->render('masterlayout', [
                    'page' => 'resetpassword/index',
                    'records' => $records,
                    'error' => $error,
		]);

    }
}
?>
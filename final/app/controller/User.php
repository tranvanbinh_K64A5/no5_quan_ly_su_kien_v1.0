<?php
class User extends Controller
{

    public $userModel;

    public $types = ['1' => 'Sinh viên', '2' => 'Giáo viên', '3' => 'Cựu sinh viên'];

    function __construct()
    {
        $this->userModel = $this->models('UserModel');
    }

    public function index()
    {
        $rs = $this->userModel->select_array();

        $this->render('masterlayout', [
            'page' => 'user/index',
            'array' => $rs,
            'types' => $this->types
        ]);
    }


    public function search()
    {
        $type = $_GET["type"];
        $keyword = $_GET["keyword"];

        if (session_status() === PHP_SESSION_NONE) session_start();
        $_SESSION["type_user"] = $type;
        $_SESSION["keyword_user"] = $keyword;

        $t = 0;

        if (!empty($type)) {
            $t = $type;
        }

        $rs = $this->userModel->searchUser($keyword, $t);

        $this->render('masterlayout', [
            'page' => 'user/index',
            'array' => $rs,
            'types' => $this->types
        ]);
    }

    public function add()
    {
        $fail = false;
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $type = $_POST['type'];
            $user_id = $_POST['user_id'];
            $description = $_POST['description'];
            $duplicate = $this->userModel->checkDuplicate($user_id);
            if (strlen($name) > 100) {
                $fail = 'Không nhập tên quá 100 ký tự';
            }
            if (!$fail and strlen($user_id) > 10) {
                $fail = 'ID chỉ nhập tối đa 10 ký tự Tiếng Anh';
            }
            if (!$fail and strlen($description) > 250) {
                $fail = 'Không nhập mô tả quá 1000 ký tự';
            }
            if (!$fail and $duplicate) {
                $fail = "Mã thành viên đã tồn tại";
            }
            if (!$duplicate and isset($_FILES['avatar'])) {

                $target_dir = __DIR__ . "/../../web/avatar/";

                $allowUpload = true;

                $fileType = pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION);
                $fileName = $user_id . '_' . $type . "." . $fileType;
                $target_file = $target_dir . $fileName;
                $urlDir = "web/avatar/";

                $maxfilesize   = 10000000;


                if ($fail == false and $_FILES["avatar"]['error'] != 0) {
                    $fail = "The uploaded file is error or no file selected.";
                    die;
                }


                if ($fail == false and $_FILES["avatar"]["size"] > $maxfilesize) {
                    $fail = "Size of the uploaded file must be smaller than $maxfilesize bytes.";
                    $allowUpload = false;
                }


                if ($fail == false and file_exists($target_file)) {
                    $fail = "The file name already exists on the server.";
                    $allowUpload = false;
                }

                if ($fail == false and $allowUpload) {
                    $created = date('Y-m-d H:i:s');
                    $updated = date('Y-m-d H:i:s');

                    if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {
                        $avatar = $urlDir . $fileName;
                        $data = array(
                            'name' => $name,
                            'user_id' => $user_id,
                            'type' => $type,
                            'description' => $description,
                            'avatar' => $avatar,
                            'created' => $created,
                            'updated' => $updated
                        );
                        $rs = $this->userModel->insert($data);
                        if (!$rs) {
                            $fail = "An error occurred while inserting the data.";
                        } else {
                            $this->add_success();
                        }
                    } else {
                        $fail = "An error occurred while uploading the file.";
                    }
                }
            }
        }
        $this->render('masterlayout', [
            'page' => 'user/add',
            'types' => $this->types,
            'fail' => $fail
        ]);
    }

    public function add_success()
    {
        $this->render('masterlayout', [
            'page' => 'user/add_success',
        ]);
    }


    public function update($id) {
        $data = $this->userModel->select_one("*",["id"=>$id]);
        $_SESSION['server_url'] = 'http://localhost/final/';
        $input_valid = true;
        $error_messages = ['name' => '', 'id' => '', 'des' => ''];
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            //xác nhận lần đầu
            if(isset($_POST['btnxacnhan'])) {
                if(empty($_POST["name_update_form"])) {
                    $error_messages['name'] = "Họ và tên không được trống!";
                    $input_valid = false;
                }
                if(empty($_POST["user_id_update_form"])) {
                    $error_messages['id'] = "ID không được trống!";
                    $input_valid = false;
                }
                if(empty(htmlentities($_POST['description_update_form']))) {
                    $error_messages['des'] = "Cần thêm mô tả!";
                    $input_valid = false;
                }
                
                if ($input_valid) {
                    if (empty($_FILES['avatar_for_update']['name'])){
                        $this->render('masterlayout', [
                            'page' => 'user/student_modifying_confirm',
                            'avatar' => $data['avatar'],
                        ]);
                    } else {
                        $_SESSION['avatar_for_update'] = $_FILES['avatar_for_update'];
                        $this->render('masterlayout', [
                            'page' => 'user/student_modifying_confirm',
                        ]);
                        
                        //thực hiện upload ảnh
                        if(!file_exists('web/avatar')){
                            mkdir('web/avatar');
                        } 
                        $target_dir = "web/avatar/";
                        $extension_file = pathinfo($_FILES['avatar_for_update']['name'], PATHINFO_EXTENSION);
                        $basename_file = pathinfo($_FILES['avatar_for_update']['name'], PATHINFO_FILENAME);
                        $target_file = $target_dir . $basename_file .'_'. date("YmdHis").'.'.$extension_file;
                        if (!file_exists($target_file)) {
                            move_uploaded_file($_FILES['avatar_for_update']["tmp_name"], $target_file);
                        }
                        $_SESSION['target_file'] = $target_file;
                    } 
    
                    //sau khi chuyển trang lưu dữ liệu vào session
                    $_SESSION["name_for_update"] = $_POST['name_update_form'];
                    $_SESSION["type_for_update"] = $_POST['type_update_form'];
                    $_SESSION["user_id_for_update"] = $_POST['user_id_update_form'];
                    $_SESSION["description_for_update"] = htmlentities($_POST['description_update_form']);
                }
            }


            //preview xác nhận lần 2 và tiến hành update
            if(isset($_POST['btnxacnhan2'])) {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                if (!empty($_SESSION['avatar_for_update'])){
                    //xóa ảnh avatar cũ
                    if(file_exists($data['avatar'])){
                        unlink($data['avatar']); 
                    } 
                    //thực hiện update
                    $this->userModel->update(
                        ["name"=>trim($_SESSION["name_for_update"]),
                        "type"=>$_SESSION["type_for_update"],
                        "user_id"=>trim($_SESSION["user_id_for_update"]),
                        "avatar"=>$_SESSION['target_file'],
                        "description"=>trim($_SESSION["description_for_update"]),
                        "updated"=> date('Y/m/d H:i:s', time()),
                        ],["id"=>$id]);
                } else {
                    $this->userModel->update(
                        ["name"=>trim($_SESSION["name_for_update"]),
                        "type"=>$_SESSION["type_for_update"],
                        "user_id"=>trim($_SESSION["user_id_for_update"]),
                        "description"=>trim($_SESSION["description_for_update"]),
                        "updated"=> date('Y/m/d H:i:s', time()),
                        ],["id"=>$id]);
                }
                
                $this->render('masterlayout', [
                    'page' => 'user/student_modifying_complete',
                ]);

                $_SESSION['target_file'] = $_SESSION['avatar_for_update'] = $_SESSION["name_for_update"] = $_SESSION["user_id_for_update"] = $_SESSION["description_for_update"] = $_SESSION["type_for_update"] = $_SESSION["avatar_for_update"] =  null;
            }

            //btn sửa lại
            if(isset($_POST['btnsualai'])) {
                echo "<script> location.href='http://localhost/final/user/update/".$id."'; </script>";
                exit;
            }
        }

        $this->render('masterlayout', [
                        'page' => 'user/student_modifying_input',
                        'data' => $data,
                        'error' => $error_messages,
                    ]);
    }


    public function delete() {
        $user_id = $_POST['user_id'];
        $rs = $this->userModel->deleteByUserId($user_id);
        if ($rs == true) {
            echo "successfully";
        }
    }
}

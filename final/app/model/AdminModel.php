<?php 
require_once _DIR_ROOT."/app/model/MyModels.php";

class AdminModel extends MyModels {
    protected $table = "admins";
    
    public function Checklogin($idlogin,$password)
    {
        return $this->select_one('login_id,password,actived_flag',['login_id'=>$idlogin,'password'=>$password,'actived_flag'=>1]);
    }
	
	public function checkLoginId($idlogin)
    {
        return $this->select_one('login_id',['login_id'=>$idlogin]);
    }
	
	public function resetPasswordToken($idlogin)
    {
		$token = microtime(true);
		$idlogin = '"' . $idlogin . '"';
        return $this->update(['reset_password_token'=>$token],['login_id'=>$idlogin]);
    }
	
	public function getRequestedUsers()
	{
		$sql = "SELECT * FROM `admins` WHERE length(reset_password_token) > 0";
		$query = $this->conn->prepare($sql);
		$query->execute();
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	public function updatePassword($idlogin,$newPass)
	{
		$newPass = md5($newPass);
		$idlogin = '"' . $idlogin . '"';
		return $this->update(['reset_password_token'=>'','password'=>$newPass],['login_id'=>$idlogin]);
	}

}
?>
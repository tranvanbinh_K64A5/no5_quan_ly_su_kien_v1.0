<?php
require_once _DIR_ROOT . "/app/model/MyModels.php";

class EventModel extends MyModels
{
    protected $table = "events";

    function searchEvent($keyword)
    {
        if ($keyword === '') {
            return [];
        }

        $sql   = "SELECT events.id, events.name, events.slogan, events.leader 
                FROM events WHERE   events.name LIKE :keyword OR 
                               events.description LIKE :keyword OR 
                               events.leader LIKE :keyword OR 
                               events.slogan LIKE :keyword
                             ORDER BY events.id DESC";

        $new_keyword = '%'.$keyword.'%';
        $query = $this->conn->prepare($sql);
        $query->bindParam(':keyword', $new_keyword);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function deleteEvent($id)
    {
//        $sql = "DELETE FROM events WHERE id = '".$id."'";
//        $sql_event_comment = "DELETE FROM event_comments WHERE event_id = '".$id."'";
//        $sql_event_timeline = "DELETE FROM event_timelines WHERE event_id = '".$id."'";
//        $query = $this->conn->prepare($sql);
//        $query1 = $this->conn->prepare($sql_event_timeline);
//        $query2 = $this->conn->prepare($sql_event_comment);
//        if ($query->execute() && $query1->execute() && $query2->execute()) {
//            return true;
//        } else return false;

        $sql   = "DELETE FROM events WHERE id = :id";
        $query = $this->conn->prepare($sql);
        $query->bindParam(':id', $id);
        if ($query->execute()) {
            return true;
        } else return false;
    }

    function getEventById($id) {
        $sql   = "SELECT * 
            FROM events 
            WHERE events.id = :id";

        $query = $this->conn->prepare($sql);
        $query->bindParam(':id', $id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }
}

?>
<?php
require_once _DIR_ROOT . "/app/model/MyModels.php";

class UserModel extends MyModels
{
    protected $table = "users";

    function searchUser($keyword, $type)
    {
        $sql = "SELECT * FROM $this->table ";

        if ($type === 0) {
            $sql .= "WHERE name LIKE :keyword OR 
                description LIKE :keyword
                ORDER BY id ASC";
        } else {
            $sql .= "WHERE type = :type AND
                (name LIKE :keyword OR 
                description LIKE :keyword)
                ORDER BY id ASC";
        }

        // echo $sql;

        $new_keyword = '%' . $keyword . '%';
        $query = $this->conn->prepare($sql);
        $query->bindParam(':keyword', $new_keyword);
        if ($type !== 0) $query->bindParam(':type', $type);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function deleteByUserId($user_id)
    {
        $sql = "DELETE FROM $this->table WHERE user_id = '" . $user_id . "'";

        $query = $this->conn->prepare($sql);
        if ($query->execute()) {
            return true;
        } else return false;
    }

    function checkDuplicate($user_id)
    {
        $sql = "SELECT * FROM $this->table WHERE user_id = '" . $user_id . "'";

        $query = $this->conn->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return count($result) > 0;
    }
}

<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>


    <!-- <?php
    // echo "Hinh anh: ".$_SESSION["event_avatar_for_update"];
    
    ?> -->


    <?php
    $_SESSION["name_update_form"] = $_POST['name_update_form'];
    $_SESSION["slogan_update_form"] = $_POST['slogan_update_form'];
    $_SESSION["leader_update_form"] = $_POST['leader_update_form'];
    $_SESSION["description_update_form"] = htmlentities($_POST['description_update_form']);
    ?>



    <form
        style="border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;"
        name="" action="" enctype="multipart/form-data" method="post">

        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 12px;">
            <p style="width: 150px; padding-top: 15px;">Tên sự kiện </p>
            <p id="name_form" style="border: 2px solid #4f7ba3; width: 180px; height: 30px;">
                <?php echo $_POST['name_update_form']; ?>
            </p>
        </div>
        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 12px;">
            <p style="width: 150px; padding-top: 15px;">Slogan </p>
            <p id="slogan_form" style="border: 2px solid #4f7ba3; width: 180px; height: 30px;"> <?php echo $_POST['slogan_update_form']; ?></p>
        </div>


        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 12px;">
            <p style="width: 150px; padding-top: 15px;">Leader</p>
            <p id="leader_form" style="border: 2px solid #4f7ba3; width: 180px; height: 30px;">
                <?php echo $_POST['leader_update_form']; ?>
            </p>
        </div>
        <div class="container" style="display: flex; flex-direction: row; margin-top: 12px;">
            <p style="width: 150px; padding-top: 15px;">Mô tả chi tiết</p>
            <p name=""
                style="border: 2px solid #4f7ba3; width: 300px; height: 150px; word-break: break-all; white-space: normal;">
                <?php echo htmlspecialchars($_POST['description_update_form']); ?></p>
        </div>
        <div class="container" style="display: flex; flex-direction: row; margin-top: 12px;">
            <p style="width: 150px; padding-top: 15px;">Avatar</p>
            <img src="<?php echo '../../' . $_SESSION["event_avatar_for_update"]; ?>" alt="IMAGE"
                style=" height: 100px; width: 130px; background-color: #cccccc;">
            <!-- <img src="<?php echo $_SESSION["avatar_for_update"]; ?>" alt="chưa có ảnh" style=" height: 40px; width: 50px;"> -->
            <!-- <img src="web/avatar/avata_20230113142437.jpg" alt="no image" style=" height: 40px; width: 50px;">  -->
        </div>





        <div style="display: flex; flex-direction: row; margin-top: 30px;margin-bottom: 20px; justify-content: center;">
            <button type="" name=""
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px;  border-radius: 8px;">
                Sửa lại </button>
            <button type="submit" name="btnxacnhansuaevent2"
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; border-radius: 8px; margin-left:30px;">
                Đăng kí </button>
        </div>


    </form>
</body>

</html>
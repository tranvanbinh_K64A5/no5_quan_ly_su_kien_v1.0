<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>


    <?php
    //những biến này để hiển thị trong ô nhập
    $name = $data['data']["name"];
    $slogan = $data['data']["slogan"];
    $leader = $data['data']["leader"];
    $avatar = $data['data']["avatar"];
    $description = $data['data']["description"];
    ?>


    <form
        style="border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;"
        name="" action="" enctype="multipart/form-data" method="post">

        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 14px;">
            <p style="width: 150px; padding-top: 15px;">Tên sự kiện </p>
            <div style="display: flex; flex-direction: column">
                <input type="text" name="name_update_form" value="<?php echo $name; ?>"
                    style="border: 2px solid #4f7ba3; height: 30px;" />
                <span style="font-size: 12px; color: red;">
                    <?php if (isset($data['error']['name_update_form'])) {
                        echo $data['error']['name_update_form'];
                    } ?>
                </span>
            </div>
        </div>

        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 14px;">
            <p style="width: 150px; padding-top: 15px;">Slogan </p>
            <div style="display: flex; flex-direction: column">
                <input type="text" name="slogan_update_form" value="<?php echo $slogan; ?>"
                    style="border: 2px solid #4f7ba3; height: 30px;" />
                <span style="font-size: 12px; color: red;">
                    <?php if (isset($data['error']['slogan_update_form'])) {
                        echo $data['error']['slogan_update_form'];
                    } ?>
                </span>
            </div>
        </div>


        <div class="container"
            style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 14px;">
            <p style="width: 150px; padding-top: 15px;">Leader</p>
            <div style="display: flex; flex-direction: column">
                <input type="text" name="leader_update_form" value="<?php echo $leader; ?>"
                    style="border: 2px solid #4f7ba3; height: 30px;" />
                <span style="font-size: 12px; color: red;">
                    <?php if (isset($data['error']['leader_update_form'])) {
                        echo $data['error']['leader_update_form'];
                    } ?>
                </span>
            </div>
        </div>
        <div class="container" style="display: flex; flex-direction: row; margin-top: 14px;">
            <p style="width: 150px; padding-top: 15px;">Mô tả chi tiết</p>
            <div style="display: flex; flex-direction: column">
                <textarea rows="5" name="description_update_form" value=""
                    style="border: 2px solid #4f7ba3; width: 300px; height: 100px;"> <?php echo $description; ?> </textarea>
                <span style="font-size: 12px; color: red;"><?php if (isset($data['error']['description_update_form'])) {
                    echo $data['error']['description_update_form'];
                } ?></span>
            </div>
        </div>
        <div class="container" style="display: flex; flex-direction: row; margin-top: 14px;">
            <p style="width: 150px; padding-top: 15px;">Avatar</p>
            <div class="container" style="display: flex; flex-direction: column;">
                <div style="display: flex; flex-direction: column">
                    <img src="<?php echo '../../' . $avatar; ?>" alt="IMAGE"
                        style=" height: 100px; width: 130px; background-color: #cccccc;">
                    <span style="font-size: 12px; color: red;">
                        <?php if (isset($data['error']['event_avatar_update_form'])) {
                            echo $data['error']['event_avatar_update_form'];
                        } ?>
                    </span>
                </div>
                <div class="container" style="display: flex; flex-direction: row; margin-top: 10px;">
                    <input type="text" name="event_avatar_update_form" value="<?php echo $avatar; ?>"
                        style="margin-left: 4px; border: 2px solid #4f7ba3; height: 30px;" />
                    <input type="file" name="event_avatar_for_update" style=" height: 30px;" />
                </div>
            </div>
        </div>



        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="submit" name="btnxacnhansuaevent"
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Xác nhận </button>
        </div>

    </form>

</body>

</html>
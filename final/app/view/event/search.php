<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="http://localhost/final/">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <title>Search</title>
</head>
<style>

    .width-500px{
        width: calc(100% - 500px);
    }

    .mt-16{
        margin-top: 16px;
    }

    .input-search-event{
        background-color: aliceblue;
        border: 1px solid #2771a4;
    }

    .input-search-event:focus-visible{
        outline: 1px solid #2771a4;
        box-shadow: rgba(17, 17, 26, 0.1) 0px 8px 24px, rgba(17, 17, 26, 0.1) 0px 16px 56px, rgba(17, 17, 26, 0.1) 0px 24px 80px;
    }

    .btn-event{
        background-color: #4F81BD;
        color: #ffffff;
        margin-right: 10px;
        padding: 4px 12px;
        border: 1px solid #0a3766;
    }

    .btn-event>a{
        color: #ffffff;
        text-decoration: none;
    }

</style>
<body>
<div class="container mt-4">
    <div class="main">
        <div class="event__form-search body__top row">
            <div class="left col-lg-4 text-center">
                <div>
                    <p>Từ khóa</p>
                </div>
            </div>

            <div class="right col-lg-8">
                <form action="event/search" method="GET">
                    <div>
                        <input type="text" name="key_search" id="" class="width-500px input-search-event" value="<?php if (isset($_SESSION["keyword_event"])) echo $_SESSION["keyword_event"] ?>">
                    </div>
                    <div class="width-500px d-flex justify-content-center  align-items-center mt-16">
                        <button type="submit" class="btn btn-primary ">Tìm kiếm</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="body__center mt-4">
            Số sự kiện tìm thấy : <span class="total"><?php echo count($data['array']);?></span>
        </div>

        <?php
        if (sizeof($data['array']) > 0){
            include_once "search_result.php";
        }
        ?>

    </div>
</div>
</body>
<script src="/final/web/js/event/index.js"></script>
</html>
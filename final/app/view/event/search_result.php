<div class="body__bottom mt-4">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Tên sự kiện</th>
            <th scope="col">Slogan</th>
            <th scope="col">Leader</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach ($data['array'] as $key => $item){ ?>
            <tr <?= $item['id'] ?>>
                <th scope="row"><?= $i ?></th>
                <td><?= $item['name'] ?></td>
                <td><?= $item['slogan'] ?></td>
                <td><?= $item['leader'] ?></td>
                <td class="d-flex">
                    <div>
                        <button class="text-nowrap btn-event" id="event<?= $item['id']?>" data-control="event" onclick="delEvent('<?= $item['id']?>', '<?= $item['name'] ?>')">Xóa</button>
                    </div>
                    <div>
                        <button class="text-nowrap btn-event" >
                            <a href="http://localhost/final/event/update/<?=$item['id']?>">Sửa</a>
                        </button>
                    </div>
                    <div>
                        <button class="text-nowrap btn-event" >
                            <a href="http://localhost/final/eventtimeline/index/<?=$item['id']?>">Lịch Trình</a>
                        </button>
                    </div>
                    <div>
                        <button class="text-nowrap btn-event" >
                            <a href="http://localhost/final/eventcomment/index/<?=$item['id']?>">Comment</a>
                        </button>
                    </div>
                </td>
            </tr>
            <?php $i++; ?>
        <?php } ?>
        </tbody>
    </table>
</div>

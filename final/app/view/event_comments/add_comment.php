<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thêm mới Comment</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/add_comment.css" />
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <form action="eventcomment/add/<?php if (isset($data['event_id'])) echo $data['event_id'] ?>" method="post" enctype="multipart/form-data">
                <span style="color: #ff3333; display: block; margin-bottom: 4px;"><?php if (isset($data['error_messages']) && $data['error_messages']['avatar'] !== '') echo $data['error_messages']['avatar'] ?></span>
                <div class="field">
                    <label for="avatar" class="field__label">Avatar</label>
                    <input type="file" name="avatar" id="avatar" class="field__input"/>
                </div>
                
                <span style="color: #ff3333; display: block; margin-bottom: 4px;"><?php if (isset($data['error_messages']) && $data['error_messages']['content'] !== '') echo $data['error_messages']['content'] ?></span>
                <div class="field">
                    <label for="content" class="field__label">Nội dung</label>
                    <textarea id="content" name="content" class="field__input"></textarea>
                </div>
                <div class="button">
                    <button type="submit" class="btn-submit" name="submit_comment" value="add">Thêm mới</button>
                </div>
            </form>
        </div>
    </div>


</body>
</html>
<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông báo</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/confirm_comment.css" />
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <span>🎉 Bạn đã thêm comment thành công!</span>
            <br>
            <br>
            <span>Trở về trang <a href="http://localhost/final/eventcomment/index/<?=$data['event_id'] ?>">Comment của Sự kiện</a></span>
        </div>
    </div>
</body>
</html>
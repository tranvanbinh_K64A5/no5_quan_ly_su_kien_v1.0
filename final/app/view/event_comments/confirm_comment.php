<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận Comment</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/confirm_comment.css" />
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <form action="eventcomment/confirm_add" method="post">
                <input type="hidden" id="event_id" name="event_id" value="<?php if (isset($data['event_id'])) echo  $data['event_id']?>">
                <input type="hidden" id="avatarUrl" name="avatarUrl" value="<?php if (isset($data['avatarUrl'])) echo  $data['avatarUrl']?>">
                <input type="hidden" id="content" name="content" value="<?php if (isset($data['content'])) echo  $data['content']?>">
                <div class="field">
                    <label for="avatar" class="field__label">Avatar</label>
                    <img src="<?php if (isset($data['avatarUrl'])) echo  $data['avatarUrl']?>" width="100" height="100">
                </div>
                <div class="field">
                    <label for="content" class="field__label">Nội dung</label>
                    <span><?php if (isset($data['content'])) echo  $data['content'] ?></span>
                </div>
                <div class="button">
                    <button type="submit" class="btn-submit" name="confirm_add" value="confirm_add">Xác nhận</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
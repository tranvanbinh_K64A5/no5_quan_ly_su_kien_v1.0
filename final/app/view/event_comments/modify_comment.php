<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/add_comment.css" />
    <style >
        .formm{
            border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;
        }
        .avatarr{
            height: 100px; width: 140px; background-color: #cccccc; margin-left: 15px;
        }
        .errors{
            font-size: 12px; color: red;
        }
        .avatarrupdate{
            margin-right: 5px; border: 2px solid #4f7ba3; height: 30px;
        }
        .title{
            width: 150px; padding-top: 15px;
        }
        .textareaa{
            border: 2px solid #4f7ba3; width: 400px; height: 100px; margin-left: -10px;
        }
    </style>
</head>

<body>
    <?php  
        $avatar = $data['data']["avatar"];
        $content = $data['data']["content"];
    ?>

    <form class="formm" 
            name="" action="" enctype="multipart/form-data" method="post">
        <div class="container" style="display: flex; flex-direction: row; margin-top: 14px;">
            <p class = "title">Avatar</p>
            <div class="container" style="display: flex; flex-direction: column; ">
                <div style="display: flex; flex-direction: column">
                    <img src="<?php echo $avatar; ?>"  alt="IMAGE" class = "avatarr">
                    <span class="errors">
                        <?php if (isset($data['error']['avatar_update_form'])) {
                            echo $data['error']['avatar_update_form'];
                        } ?>
                    </span>
                </div>
                <div class="container" style="display: flex; flex-direction: row; margin-top: 10px;">
                    <input type="text" name="avatar_update_form" value="<?php echo $avatar; ?>" class = "avatarrupdate" />
                    <input type="file" name="avatar_for_update" style=" height: 30px;" />
                </div>
            </div>
        </div>

    <div class="container" style="display: flex; flex-direction: row; margin-top: 14px;">
            <p class="title">Nội dung</p>
            <div style="display: flex; flex-direction: column">
                <textarea rows="5" name="content_update_form" value="" class="textareaa"> <?php echo $content; ?> </textarea>
                <span class="errors"><?php if (isset($data['error']['content_update_form'])) {
                    echo $data['error']['content_update_form'];
                } ?></span>
            </div>
        </div>

        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="submit" name="btnxacnhansua" class=" btn-submit" 
                style=" margin: auto; ">
                Sửa </button>

   
        </div>
         
    </form>

</body>
</html>



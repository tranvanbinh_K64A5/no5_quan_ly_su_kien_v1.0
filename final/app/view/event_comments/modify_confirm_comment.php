<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="web/css/event_comments/confirm_comment.css" />
    <style>
        .btn-submit{
    font-size: 14px;
    color: #fff9fb;
    background-color: #4991c0;
    padding: 12px 32px;
    margin-top: 15px;
    border: none;
    cursor: pointer;
    border-radius: 10px;
    transition: ease-in-out 0.3s;
    }
    .content{
        border: 2px solid #4f7ba3; 
        width: 400px; 
        height: 100px; 
        margin-left: -1px
    }
    .title{
        width: 150px; padding-top: 15px;
    }
    .img_src {
         height: 100px; width: 140px; background-color: #cccccc;
    }
    .formm{
       border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;
    }
    </style>
    
</head>

<body>
 <!--    <?php 
        echo "Hinh anh: ".$_SESSION["avatar_for_update"];
    ?> -->
    <?php 
        $_SESSION["content_update_form"] = htmlentities($_POST['content_update_form']);
    ?>
    <form class="formm" 
            name="" action="" enctype="multipart/form-data" method="post">
    <div class="container" style="display: flex; flex-direction: row; margin-top: 12px;">
            <p class= 'title'>Avatar</p>
            <img src="<?php echo '../../' . $_SESSION["avatar_for_update"]; ?>" alt="IMAGE" class = "img_src">
        </div>
    <div class="container" style="display: flex; flex-direction: row; margin-top: 12px;">
            <p class="title">Nội dung</p>
            <p name="" class="content" >
                <?php echo htmlspecialchars($_POST['content_update_form']); ?></p>
        </div> 
        <div style="display: flex;align-items: center; margin-top: 20px; ">
            <button type="submit" class="btn-submit" name="btnxacnhansuacomment"
                style=" margin: auto; ">
                Xác nhận sửa </button>
        </div>
    </form>
</body>
</html>





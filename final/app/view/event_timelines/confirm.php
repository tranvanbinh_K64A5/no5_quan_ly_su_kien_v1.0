<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận Timeline</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/confirm_comment.css" />
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <form action="eventtimeline/confirm_add" method="post"> 
                <input type="hidden" id="event_id" name="event_id" value="<?php if (isset($data['event_id'])) echo  $data['event_id']?>"> 
                <input type="hidden" id="start_date" name="start_date" value="<?php if (isset($data["data"]['start_date'])) echo  $data["data"]['start_date']?>">
                <input type="hidden" id="end_date" name="end_date" value="<?php if (isset($data["data"]['end_date'])) echo  $data["data"]['end_date']?>">
                <input type="hidden" id="name" name="name" value="<?php if (isset($data["data"]['name'])) echo  $data["data"]['name']?>">
                <input type="hidden" id="description" name="description" value="<?php if (isset($data["data"]['description'])) echo  $data["data"]['description']?>">
                <input type="hidden" id="poc_name" name="poc_name" value="<?php if (isset($data["data"]['poc_name'])) echo  $data["data"]['poc_name']?>"> 
                <div class="field">
                    <label for="content" class="field__label">Bắt đầu</label>
                    <span><?php if (isset($data["data"]['start_date'])) echo  $data["data"]['start_date'] ?></span>
                </div>
                <div class="field">
                    <label for="content" class="field__label">Kết thúc</label>
                    <span><?php if (isset($data["data"]['end_date'])) echo  $data["data"]['end_date'] ?></span>
                </div>
                <div class="field">
                    <label for="content" class="field__label">Tên lịch trình</label>
                    <span><?php if (isset($data["data"]['name'])) echo  $data["data"]['name'] ?></span>
                </div>
                <div class="field">
                    <label for="content" class="field__label">Nội dung</label>
                    <span><?php if (isset($data["data"]['description'])) echo  $data["data"]['description'] ?></span>
                </div>
                <div class="field">
                    <label for="content" class="field__label">Người chịu trách nhiệm</label>
                    <span><?php if (isset($data["data"]['poc_name'])) echo  $data["data"]['poc_name'] ?></span>
                </div>
                <div class="button">
                    <button type="submit" class="btn-submit" name="confirm_add" value="confirm_add">Xác nhận</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thêm mới Timeline</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/event_comments/add_comment.css" />
</head>
<body>
    <div class="main">
        <div class="wrapper">
            <form action="eventtimeline/add/<?php if (isset($data['event_id'])) echo $data['event_id'] ?>" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label for="start_date" class="field__label">Bắt đầu</label>
                    <input id="start_date" name="start_date"  type="datetime-local" class="field__input">
                </div>
                <div class="field">
                    <label for="end_date" class="field__label">Kết thúc</label>
                    <input id="end_date" name="end_date"  type="datetime-local" class="field__input">
                </div>
                <div class="field">
                    <label for="name" class="field__label">Tên lịch trình</label>
                    <input id="name" name="name"  type="text" class="field__input">
                </div>
                <div class="field">
                    <label for="description" class="field__label">Nội dung</label>
                    <textarea id="description" name="description" class="field__input" placeholder="Nội dung..."></textarea>
                </div>
                <div class="field">
                    <label for="poc_name" class="field__label">Người chịu trách nhiệm</label>
                    <input id="poc_name" name="poc_name"  type="text" class="field__input">
                </div>
                <span style="color: #ff3333; display: block; margin-bottom: 4px;">
                    <?php if (isset($data['error_messages']) && $data['error_messages']['start_date'] !== '') echo $data['error_messages']['start_date'] ?>
                </span> 
                <span style="color: #ff3333; display: block; margin-bottom: 4px;">
                    <?php if (isset($data['error_messages']) && $data['error_messages']['end_date'] !== '') echo $data['error_messages']['end_date'] ?>
                </span> 
                <span style="color: #ff3333; display: block; margin-bottom: 4px;">
                    <?php if (isset($data['error_messages']) && $data['error_messages']['name'] !== '') echo $data['error_messages']['name'] ?>
                </span> 
                <span style="color: #ff3333; display: block; margin-bottom: 4px;">
                    <?php if (isset($data['error_messages']) && $data['error_messages']['description'] !== '') echo $data['error_messages']['description'] ?>
                </span> 
                <span style="color: #ff3333; display: block; margin-bottom: 4px;">
                    <?php if (isset($data['error_messages']) && $data['error_messages']['poc_name'] !== '') echo $data['error_messages']['poc_name'] ?>
                </span> 
                <div class="button">
                    <button type="submit" class="btn-submit" name="submit_timeline" value="add">Thêm mới</button>
                </div>
            </form>
        </div>
    </div>


</body>
</html>
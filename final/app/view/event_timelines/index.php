<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Timeline</title>
    <base href="http://localhost/final/"> 
    <link rel="stylesheet" href="web/css/user/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</head>
<body>
<div class="main">
    <div class="wrapper">
        <a href="/final">
            <button type="button" class="btn-submit">
                <div style="display: flex; ">
                    <i class="material-icons" style="font-size: 18px; margin-top: -1px; margin-right: 4px">home</i>
                    <span>Trang chủ</span>
                </div>
            </button>
        </a>

        <div class="result">
            <div>
                <span>Tên sự kiện: <span class="event_name" style="font-weight: 600;"><?php if (isset($data['event']['name'])) echo $data['event']['name'] ?></span></span>
            </div>

            <div>
                <a href="eventtimeline/add/<?= $data['event']['id'] ?>">
                    <button type="button" class="btn-submit">
                        <div style="display: flex; ">
                            <i class="material-icons" style="font-size: 18px; margin-top: -1px; margin-right: 4px">add</i>
                            <span>Thêm Lịch trình</span>
                        </div>
                    </button>
                </a>
            </div>
        </div>

        <div class="list-student">
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên</th>
                    <th>Nội dung Lịch trình</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Action</th>
                </tr>

                <?php
                    $i = 1;
                    foreach($data['timelines'] as $key => $value) {?>
                        <tr>
                            <td><?= $i ?></td>
                            <td style="padding-right: 0"><?= $value['name'] ?></td>
                            <td><?= $value['detail'] ?></td>
                            <td><?= $value['schedule_from'] ?></td>
                            <td><?= $value['schedule_to'] ?></td>
                            <td>
                                <div class="actions">
                                    <a href="eventtimeline/update/<?= $value['id'] ?>"><button class="btn-action">Sửa</button></a>
                                </div>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php } ?>

            </table>
        </div>

    </div>
</div>
</body>
</html>

<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://localhost/final/">
    <title>Thêm mới Comment</title>
</head>
<body>
    <h1>Sửa lịch trình</h1>
    <?php  
        $id = $data['data']["id"];
        $from = $data['data']["schedule_from"];
        $to = $data['data']["schedule_to"];
        $name = $data['data']["name"];
        $detail = $data['data']["detail"];
        $poc_name = $data['data']["PoC"];
    ?>
    <div class="main">
        <div class="wrapper">
            <form action="eventtimeline/update/<?php echo $data['data']["id"] ?>" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label for="start_date" class="field__label">Bắt đầu</label>
                    <input id="start_date" name="start_date"  type="datetime-local" class="field__input" value ="<?php echo $from; ?>">
                </div>
                <div class="field">
                    <label for="end_date" class="field__label">Kết thúc</label>
                    <input id="end_date" name="end_date"  type="datetime-local" class="field__input" value ="<?php echo $to; ?>">
                </div>
                <div class="field">
                    <label for="name" class="field__label">Tên lịch trình</label>
                    <input id="name" name="name"  type="text" class="field__input" value ="<?php echo $name; ?>">
                </div>
                <div class="field">
                    <label for="detail" class="field__label">Nội dung</label>
                    <textarea id="detail" name="detail" class="field__input"><?php echo $detail;?></textarea>
                </div>
                <div class="field">
                    <label for="poc_name" class="field__label">Người chịu trách nhiệm</label>
                    <input id="poc_name" name="poc_name"  type="text" class="field__input" value ="<?php echo $poc_name; ?>">
                </div>
                <div class="button">
                    <button type="submit" class="btn-submit" name="submit_modify" value="add">Sửa</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
<style>
    * {
    font-size: 100%;
    font-family: Arial;
}

*:focus {
    outline: none;
}

.main {
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 80vh;
}

.wrapper {
    width: 30%;
    padding: 40px;
    border: 1px solid #5b9bd5;
}

.field {
    margin-bottom: 25px;
    display: flex;
    align-items: center;
    justify-content: space-between;
}

.field__label {
    color: #444444;
    width: 30%;
}
.field__input {
    font-size: 14px;
    font-weight: 400;
    padding: 10px;
    border-radius: 0;
    border: 1px solid #5b9bd5;
    width: 65%;
}

.button {
    display: flex;
    justify-content: center;
    align-items: center;
}

.btn-submit {
    font-size: 14px;
    color: #fff9fb;
    background-color: #4991c0;
    padding: 12px 32px;
    margin-top: 15px;
    border: none;
    cursor: pointer;
    border-radius: 10px;
    transition: ease-in-out 0.3s;
}

.btn-submit:hover {
    background-color: #2a5a79;
}
</style>
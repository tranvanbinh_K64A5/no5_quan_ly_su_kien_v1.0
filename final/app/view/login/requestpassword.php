<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <base href="http://localhost/final/">
	<link rel="stylesheet" href="./web/css/login_home/login.css" type="text/css" />
	<style>
	</style>
    <title>Yêu cầu reset password</title>
</head>
<body>
<div style="height: 350px; width: 550px;  margin: 0 auto; border: 1px solid #5b9bd5; display: flex; justify-content:center;  flex-direction: column; align-items: center;" class="container_login">
	<form action="" method="post">
		 <div class="form-container" style="width: 450px; margin-left:255px;">
			<label style="color: red">
			<?php 
			// echo $data['error_id'];
			if (isset($data['error_id'])) {
				echo $data['error_id'];
			}
			?>
			</label>
		</div> 
		<div class="form-container">
			<div class="form-label" style="width: 110px;"><label>Người dùng</label></div>
			<input type="text" name="account" class="input-label" placeholder="" autofocus value="<?php echo isset($_POST["account"]) ? $_POST["account"] : ''; ?>">
		</div>
		
		<div class="button-container" style="margin-top: 20px;">
			<input style="width: 200px;" type="submit" value="Gửi yêu cầu reset password" class="button" name="submit_request" style="cursor: pointer">
		</div>
	</form>
</div>

</body>
</html>
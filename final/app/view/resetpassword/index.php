<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://localhost/final/">
	<style>
	.input-label-380 {
		margin-left: 20px;
		height: 2em;
		width: 15em;
		border: 1px solid #42719b;
	}
	
	.button-380 {
		height: 2em;
		width: 6em;
		background-color: #5b9bd5;
		border-radius: 5px;
		color: white;
	}
	</style>
    <title>Đăng nhập</title>
</head>
<body>
<div style="height: 350px; width: 550px;  margin: 0 auto; border: 1px solid #5b9bd5; display: flex; justify-content:center;  flex-direction: column; align-items: center;" class="container_login">
	<form action="" method="post">
		 <table>
            <thead>
                <tr>
                    <th> NO </th>
                    <th> Tên người dùng </th>
                    <th> Mật khẩu mới </th>
                    <th> Action </th>
                </tr>
            </thead>
            <tbody>
				<?php 
				
				$stt = 1;
				$records = $data['records'];
				$error = $data['error'];
				if (!empty($records)) {
					foreach ($records as $row) {
						$userinput = $row["login_id"];
						$resetbtn = 'reset_'.$row["login_id"];
						$prev_pass = isset($_POST[$userinput]) ? $_POST[$userinput] : "";
						$error_pass = isset($error[$userinput]) ? $error[$userinput] : "";
						echo '
						<tr>
							<td>' . $stt . '</td>
							<td>' . $row["login_id"] . '</td>
							<td> <input class="input-label-380" type="password" name="' . $userinput . '" placeholder="" autofocus value="'.$prev_pass.'"> </td>
							<td> <input class="button-380" type="submit" value="Reset" name="' . $resetbtn . '"> </td>
						</tr>

						<tr>
							<td>  </td>
							<td></td>
							<td><label style="color: red">'. $error_pass .'</label></td>
							<td></td>
						</tr>
						';
						$stt = $stt+1;
					}
				}
				
				?>
            </tbody>
        </table>
	</form>
</div>

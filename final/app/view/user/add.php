<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Người dùng</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/user/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</head>

<body>
    <div class="main">
        <div class="wrapper">
            <a href="/final">
                <button type="button" class="btn-submit">
                    <div style="display: flex; ">
                        <i class="material-icons" style="font-size: 18px; margin-top: -1px; margin-right: 4px">home</i>
                        <span>Trang chủ</span>
                    </div>
                </button>
            </a>
            <p style="font-weight: bold;color: red;text-align: center;">
                <?php
                    echo $data['fail'];
                ?>
            </p>
            <div class="form-search">
                <form method="post" enctype="multipart/form-data" onsubmit="return confirm('Bạn có muốn thêm thành viên này không?')">
                    <div class="field">
                        <label for="name" class="field__label">Họ và Tên</label>
                        <input type="text" name="name" id="name" class="field__input" required />
                    </div>
                    <div class="field">
                        <label for="keyword" class="field__label">Đối tượng</label>
                        <?php
                        $selected = 'checked';
                        foreach ($data['types'] as $key => $value) {
                            echo '<input ' . $selected . ' type="radio" name="type" value="' . $key . '"/>' . $value;
                            $selected = '';
                        }
                        ?>
                    </div>
                    <div class="field">
                        <label for="user_id" class="field__label">ID</label>
                        <input type="text" name="user_id" id="user_id" class="field__input" required />
                    </div>
                    <div class="field">
                        <label for="description" class="field__label">Mô tả thêm</label>
                        <textarea name="description" id="description" class="field__input" rows="4" required></textarea>
                    </div>
                    <div class="field">
                        <label for="avatar" class="field__label">Avatar</label>
                        <input type="file" accept="image/*" name="avatar" id="avatar" required />
                    </div>
                    <div class="field">
                        <img id="preview" src="/final/web/avatar/empty_avatar.jpg" alt="preview" style="width: 150px;" />
                    </div>
                    <div class="button">
                        <input type="submit" name="submit" class="btn-submit" value="Xác nhận" required />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $("#avatar").change(function() {
                readURL(this);
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>

</html>
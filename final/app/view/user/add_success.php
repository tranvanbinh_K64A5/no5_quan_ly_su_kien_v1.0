<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thành công</title>
    <base href="http://localhost/final/">
    <link rel="stylesheet" href="web/css/user/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
</head>

<body>
    <div class="main">
        <div class="wrapper">
            <div class="form-search" style="display: block;text-align: center;">
                <h2>Bạn đã đăng ký thành công thành viên</h2>
                <a href="/final">Trở về trang chủ</a>
            </div>
        </div>
    </div>
</body>

</html>
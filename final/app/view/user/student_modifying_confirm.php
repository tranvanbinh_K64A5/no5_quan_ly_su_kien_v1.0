<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <?php
        $phanloai = array(2 => 'Giáo viên', 1=> 'Sinh viên', 3=> "Cựu sinh viên");
    ?>

    <form style="border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;"
            name="" action="" enctype="multipart/form-data" method="post">

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px;"> 
            <p style = "width: 150px; padding-top: 15px;">Họ và Tên </p> 
            <p id="name_form"  style = "border: 2px solid #4f7ba3; width: 180px; height: 30px;"> <?php echo $_POST['name_update_form']; ?></p>
        </div>

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 5px;"> 
            <p style = "width: 150px; padding-top: 15px;">Phân loại </p>
            <p id= "type_form" style = "border: 2px solid #4f7ba3; width: 180px; height: 30px;"> <?php echo $phanloai[$_POST['type_update_form']]; ?></p>
        </div>

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 5px;">  
            <p style = "width: 150px; padding-top: 15px;">ID</p> 
            <p id = "user_id_form"  style = "border: 2px solid #4f7ba3; width: 180px; height: 30px;"> <?php echo $_POST['user_id_update_form']; ?></p>
        </div>

        <div class="container" style="display: flex; flex-direction: row; margin-top: 10px;"> 
            <p style = "width: 150px; padding-top: 15px;">Avatar</p> 
            <?php
                if (!empty($data["avatar"])){
                    echo '<img src="'.$_SESSION["server_url"].$data["avatar"].'" alt="IMAGE" style=" height: 100px; width: 130px; background-color: #cccccc;">';
                } else { 
                    $imageData = file_get_contents($_SESSION['avatar_for_update']['tmp_name']);
                    echo sprintf('<img src="data:image/png;base64,%s" alt="IMAGE" style=" height: 100px; width: 130px; background-color: #cccccc;"/>', base64_encode($imageData));
                }
            ?>
        </div>

        <div class="container" style="display: flex; flex-direction: row; margin-top: 10px;"> 
            <p style = "width: 150px; padding-top: 15px;">Mô tả thêm</p> 
            <p name=""  style = "border: 2px solid #4f7ba3; width: 300px; height: 150px; word-break: break-all; white-space: normal;"> <?php echo htmlspecialchars($_POST['description_update_form']); ?></p >
        </div>


        <div style="display: flex; flex-direction: row; margin-top: 30px;margin-bottom: 20px; justify-content: center;">
            <button type="" name="btnsualai"
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px;  border-radius: 8px;">
                Sửa lại </button>
            <button type="submit" name="btnxacnhan2"
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; border-radius: 8px; margin-left:30px;">
                Đăng kí </button>
        </div>


    </form>
</body>
</html>








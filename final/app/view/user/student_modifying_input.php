<?php if (!isset($_SESSION['authen'])) header('location: http://localhost/final/login'); ?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="web/css/user/file.css">
</head>

<body>


    <?php  
        //những biến này để hiển thị trong ô nhập
        $phanloai = array(2 => 'Giáo viên', 1=> 'Sinh viên', 3=> "Cựu sinh viên");

        $id = $data['data']["id"];
        $current_phanloai = $data['data']["type"];
        $name = $data['data']["name"];
        $user_id = $data['data']["user_id"];
        $avatar = $data['data']["avatar"];
        $description = $data['data']["description"];
        $error = $data['error'];
    ?>

    <form style="border: 2px solid #4f7ba3; width: 700px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 10px;"
            name="" action="" enctype="multipart/form-data" method="post">

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px;"> 
            <p style = "width: 150px; padding-top: 15px;">Họ và Tên </p> 
            <input type="text" name="name_update_form" value ="<?php echo $name; ?>" maxlength="100" minlength="4" style = "border: 2px solid #4f7ba3; height: 30px;"> </input>
            <?php 
                if (!empty($data['error']['name'])){
                    echo '<p style="color: red; margin-left: 10px; margin-top: 15px;">'.$data['error']['name'].'</p>';
                }
            ?>
        </div>

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 5px;"> 
            <p style = "width: 150px; padding-top: 15px;">Phân loại </p>
            <div style='width: 430px;'>
                <?php 
                    for ($i = 1; $i < 4; $i++){
                    if ($current_phanloai == $i){
                        echo '<input type="radio"  name="type_update_form"  value = "',$i,'" checked> 
                      <label style="margin-right: 10px;">',$phanloai[$i],'</label>';
                    } else {
                        echo '<input type="radio"  name="type_update_form"  value = "',$i,'"> 
                      <label style="margin-right: 10px;">',$phanloai[$i],'</label>';
                        }
                    }
                ?>
            </div>
        </div>

        <div class="container" style="display: flex; flex-direction: row;  align-items: center; height: 35px; margin-top: 5px;">  
            <p style = "width: 150px; padding-top: 15px;">ID</p> 
            <input type="text" name="user_id_update_form" value ="<?php echo $user_id; ?>" maxlength="10" style = "border: 2px solid #4f7ba3; height: 30px;"> </input>
            <?php 
                if (!empty($data['error']['id'])){
                    echo '<p style="color: red; margin-left: 10px; margin-top: 15px;">'.$data['error']['id'].'</p>';
                }
            ?>
        </div>

        <div class="container" style="display: flex; flex-direction: row; margin-top: 10px;"> 
            <p style = "width: 150px; padding-top: 15px;">Avatar</p> 
            <div class = "container" style="display: flex; flex-direction: column;">
                <img  id="preview" src="<?php echo $_SESSION['server_url'].$avatar; ?>" alt="IMAGE" style=" height: 100px; width: 130px; background-color: #cccccc; margin-left: 16px;">
                    <input type="file" name="avatar_for_update" id="ima" style="display:none;" accept="image/png, image/jpeg" />
                    <label for="ima" >
                        <div class="container" style="display: flex; flex-direction: row;  margin-top: 16px; margin-left: 4px;  "> 
                            <p style = "border: 2px solid #4f7ba3; height: 30px; width: 130px; text-align: center;">Chọn hình ảnh</p>
                            <p style = "border: 2px solid #4f7ba3; background-color: #4c7ae6; height: 30px; width: 60px; text-align: center; color: white; ">Browse</p>
                        </div>
                    </label>
            </div>
        </div>

        <div class="container" style="display: flex; flex-direction: row; margin-top: 5px;"> 
            <p style = "width: 150px; padding-top: 15px;">Mô tả thêm</p> 
            <textarea   rows="5" name="description_update_form" value ="" maxlength="1000" style = "border: 2px solid #4f7ba3; width: 300px; height: 100px;"> <?php echo $description; ?> </textarea >
            <?php 
                if (!empty($data['error']['des'])){
                    echo '<p style="color: red; margin-left: 10px; margin-top: 15px;">'.$data['error']['des'].'</p>';
                }
            ?>
        </div>

        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="submit" name="btnxacnhan"
                style="background-color: #4c7ae6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Xác nhận </button>
        </div>

    </form>


    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $("#ima").change(function() {
                readURL(this);
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>
</html>
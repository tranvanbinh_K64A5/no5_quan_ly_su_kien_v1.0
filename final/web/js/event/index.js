
function delEvent(id, nameEvent) {
    console.log(id, nameEvent)
    const control = $('#event' + id).attr('data-control');
    Swal.fire({
        text: `Bạn chắc chắn muốn xóa sự kiện ` + nameEvent + `?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url:control + '/delete',
                method: "post",
                data: {id},
                // dataType: 'json',
                success: function(response) {
                    if (response === 'successfully') {
                        Swal.fire(
                            'Đã xóa!',
                            'Bạn đã xóa thành công sự kiện '+ nameEvent +`!`,
                            'success'
                        );
                        $('.event' + id).remove();
                        setTimeout(()=>{
                            location.reload();
                        },2000)
                    }else{
                        Swal.fire(
                            'Lỗi !',
                            'Hãy thử xóa lại sự kiện '+ nameEvent +`!`,
                            'error'
                        );
                    }
                }
            });
        }
    })
}